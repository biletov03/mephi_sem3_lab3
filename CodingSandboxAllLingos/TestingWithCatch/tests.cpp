#include <string>
#include "catch.hpp"
#include "../../../mephi_sem2_lab2/array_sequence.hpp"
#include "../../btree.hpp"
#include "../../IDictionary.hpp"

using namespace std;


int compare_int(const int& i, const int& j) {
    return i - j;
}

TEST_CASE("Sequence test", "[Sequence]") {
   SECTION("Append") {
      Sequence<int> *arr = new ArraySequence<int>();
      for (int i = 0; i < 50; ++i) {
          arr->Append(i);
      }

      REQUIRE(arr->GetLength() == 50);

      for (int i = 0; i < 50; ++i) {
          REQUIRE(arr->Get(i) == i);
      }
   }

    SECTION("Prepend") {
       Sequence<int> * arr = new ArraySequence<int>();
       for (int i = 0; i < 50; ++i) {
           arr->Prepend(i);
       }

       REQUIRE(arr->GetLength() == 50);

       for (int i = 0; i < 50; ++i) {
           REQUIRE(arr->Get(49 - i) == i);
       }
   }

    SECTION("Copy") {
        Sequence<int> *arr = new ArraySequence<int>();
        for (int i = 0; i < 50; ++i) {
            arr->Append(i);
        }
        auto arr_copy = arr->Copy();

        REQUIRE(arr_copy->GetLength() == 50);
        for (int i = 0; i < 50; ++i) {
            REQUIRE(arr_copy->Get(i) == i);
        }
   }
}

TEST_CASE("BTree_IS", "Insert and search") {
    SECTION("Insert and search") {
        auto root = new BTree<int>(3, compare_int);
        for (int i = 0; i < 1000; ++i) {
            root->Insert((i + 233) % 1000);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(root->Search(i) == 1);
        }
    }
}

TEST_CASE("BTree_R", "Remove and max, min") {
    SECTION("Remove and max, min") {
        auto root = new BTree<int>(3, compare_int);
        for (int i = 0; i < 1000; ++i) {
            root->Insert(i % 1000);
            REQUIRE(root->Get_max() == i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(root->Search(i) == 1);
            REQUIRE(root->Get_min() == i);
            root->Remove(i);
            REQUIRE(root->Search(i) == 0);
        }
    }
}

TEST_CASE("BTree_D", "Different degrees") {
    SECTION("Degree == 2") {
        auto root_2 = new BTree<int>(2, compare_int);
        for (int i = 0; i < 1000; ++i) {
            root_2->Insert(i % 1000);
            REQUIRE(root_2->Get_max() == i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(root_2->Search(i) == 1);
            REQUIRE(root_2->Get_min() == i);
            root_2->Remove(i);
            REQUIRE(root_2->Search(i) == 0);
        }
    }
    SECTION("Degree == 4") {
        auto root_4 = new BTree<int>(4, compare_int);
        for (int i = 0; i < 1000; ++i) {
            root_4->Insert(i % 1000);
            REQUIRE(root_4->Get_max() == i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(root_4->Search(i) == 1);
            REQUIRE(root_4->Get_min() == i);
            root_4->Remove(i);
            REQUIRE(root_4->Search(i) == 0);
        }
    }
    SECTION("Degree == 5") {
        auto root_5 = new BTree<int>(5, compare_int);
        for (int i = 0; i < 1000; ++i) {
            root_5->Insert(i % 1000);
            REQUIRE(root_5->Get_max() == i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(root_5->Search(i) == 1);
            REQUIRE(root_5->Get_min() == i);
            root_5->Remove(i);
            REQUIRE(root_5->Search(i) == 0);
        }
    }
}

TEST_CASE("BTree_G_A", "GET_all") {
    SECTION("Get") {
        auto root = new BTree<Pair<int, int>>(3, compare_pair_int);
        Pair<int, int> pair;
        for (int i = 0; i < 1000; ++i) {
            pair.key = i;
            pair.value = 1000 - i;
            root->Insert(pair);
        }
        auto res = root->Get_all();
        auto check = new ArraySequence<Pair<int, int>>(1000);

        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            pair.key = i;
            pair.value = 1000 - i;
            check->Set(pair, i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(res->Get(i).key == check->Get(i).key);
            REQUIRE(res->Get(i).value == check->Get(i).value);
        }
    }
}

TEST_CASE("BTree_S", "Get size") {
    auto root = new BTree<int>(3, compare_int);
    for (int i = 0; i < 1000; ++i) {
        root->Insert((i + 233) % 1000);
        REQUIRE(root->Get_size() == i + 1);
    }

    for (int i = 0; i < 1000; ++i) {
        root->Remove(i);
        REQUIRE(root->Get_size() == 999 - i);
    }
}

TEST_CASE("IDictionary", "Constructors") {
    SECTION("NULL and add") {
        auto dic = new IDictionary<int, int>(3, compare_pair_int);
        Pair<int, int> p{};
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            REQUIRE(dic->ContainsKey(i) == 0);
            dic->Add(i, j);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->Get(i) == 1000 - i);
            REQUIRE(dic->GetCount() == i + 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic->Add(i, i);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->GetCount() == 1000);
            REQUIRE(dic->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic->ContainsKey(i) == 1);
            dic->Remove(i);
            REQUIRE(dic->ContainsKey(i) == 0);
            REQUIRE(dic->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic->Add(i, i);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->GetCount() == i + 1);
            REQUIRE(dic->Get(i) == i);
        }
    }
    SECTION("Copy form Dictionary") {
        auto dic = new IDictionary<int, int>(3, compare_pair_int);
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            dic->Add(i, j);
        }
        auto dic_2 = new IDictionary<int, int>(dic);
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->Get(i) == 1000 - i);
        }
        REQUIRE(dic_2->GetCount() == 1000);

        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == 1000);
            REQUIRE(dic_2->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            dic_2->Remove(i);
            REQUIRE(dic_2->ContainsKey(i) == 0);
            REQUIRE(dic_2->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == i + 1);
            REQUIRE(dic_2->Get(i) == i);
        }
    }

    SECTION("Copy from BTree") {
        auto root = new BTree<Pair<int, int>>(3, compare_pair_int);
        Pair<int, int> p{};
        for (int i = 0; i < 1000; ++i) {
            p.key = i;
            p.value = 1000 - i;
            root->Insert(p);
        }
        auto dic_2 = new IDictionary<int, int>(root);
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->Get(i) == 1000 - i);
        }
        REQUIRE(dic_2->GetCount() == 1000);
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == 1000);
            REQUIRE(dic_2->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            dic_2->Remove(i);
            REQUIRE(dic_2->ContainsKey(i) == 0);
            REQUIRE(dic_2->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == i + 1);
            REQUIRE(dic_2->Get(i) == i);
        }
    }
}

TEST_CASE("IDictionary_2", "Constructors_other_degree") {
    SECTION("NULL and add") {
        auto dic = new IDictionary<int, int>(2, compare_pair_int);
        Pair<int, int> p{};
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            REQUIRE(dic->ContainsKey(i) == 0);
            dic->Add(i, j);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->Get(i) == 1000 - i);
            REQUIRE(dic->GetCount() == i + 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic->Add(i, i);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->GetCount() == 1000);
            REQUIRE(dic->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic->ContainsKey(i) == 1);
            dic->Remove(i);
            REQUIRE(dic->ContainsKey(i) == 0);
            REQUIRE(dic->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic->Add(i, i);
            REQUIRE(dic->ContainsKey(i) == 1);
            REQUIRE(dic->GetCount() == i + 1);
            REQUIRE(dic->Get(i) == i);
        }
    }
    SECTION("Copy form Dictionary") {
        auto dic = new IDictionary<int, int>(4, compare_pair_int);
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            dic->Add(i, j);
        }
        auto dic_2 = new IDictionary<int, int>(dic);
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->Get(i) == 1000 - i);
        }
        REQUIRE(dic_2->GetCount() == 1000);

        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == 1000);
            REQUIRE(dic_2->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            dic_2->Remove(i);
            REQUIRE(dic_2->ContainsKey(i) == 0);
            REQUIRE(dic_2->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == i + 1);
            REQUIRE(dic_2->Get(i) == i);
        }
    }

    SECTION("Copy from BTree") {
        auto root = new BTree<Pair<int, int>>(5, compare_pair_int);
        Pair<int, int> p{};
        for (int i = 0; i < 1000; ++i) {
            p.key = i;
            p.value = 1000 - i;
            root->Insert(p);
        }
        auto dic_2 = new IDictionary<int, int>(root);
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->Get(i) == 1000 - i);
        }
        REQUIRE(dic_2->GetCount() == 1000);
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == 1000);
            REQUIRE(dic_2->Get(i) == i);
        }
        for(int i = 0; i < 1000; ++i) {
            REQUIRE(dic_2->ContainsKey(i) == 1);
            dic_2->Remove(i);
            REQUIRE(dic_2->ContainsKey(i) == 0);
            REQUIRE(dic_2->GetCount() == 1000 - i - 1);
        }
        for (int i = 0; i < 1000; ++i) {
            dic_2->Add(i, i);
            REQUIRE(dic_2->ContainsKey(i) == 1);
            REQUIRE(dic_2->GetCount() == i + 1);
            REQUIRE(dic_2->Get(i) == i);
        }
    }
}

TEST_CASE("IDictionary_Get", "Get_all") {
    SECTION("Get") {
        auto dic = new IDictionary<int, int>(3, compare_pair_int);
        Pair<int, int> p{};
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            dic->Add(i, j);
        }
        auto res = dic->Get_all();
        auto check = new ArraySequence<Pair<int, int>>(1000);
        Pair<int, int> pair;
        for (int i = 0; i < 1000; ++i) {
            int j = 1000 - i;
            pair.key = i;
            pair.value = 1000 - i;
            check->Set(pair, i);
        }
        for (int i = 0; i < 1000; ++i) {
            REQUIRE(res->Get(i).key == check->Get(i).key);
            REQUIRE(res->Get(i).value == check->Get(i).value);
        }
    }
}