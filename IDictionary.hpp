#ifndef MEPHI_SEM3_LAB2_IDICTIONARY_HPP
#define MEPHI_SEM3_LAB2_IDICTIONARY_HPP

#include "btree.hpp"
template <typename TKey, typename TElement>
struct Pair {
    TKey key;
    TElement value;

    Pair(TKey _key, TElement _value) {
        key = _key;
        value = _value;
    }

    Pair() = default;
};
//template <typename T>
int compare_pair_int(const Pair<int, int> &i, const Pair<int, int> &j) {
    return i.key - j.key;
}
int compare_str_pair(const Pair<std::string, ArraySequence<int> *>& i, const Pair<std::string, ArraySequence<int> *>& j) {
    return i.key.compare(j.key);
}
template <typename TKey, typename TElement>
class IDictionary {
private:
    BTree<Pair<TKey, TElement>> * root;
    size_t size;
public:
    IDictionary(size_t _degree, int (*cmp)(const Pair<TKey, TElement> &i, const Pair<TKey, TElement> &j)) {
        root = new BTree<Pair<TKey, TElement>>(_degree, cmp);
        size = 0;
    }
    explicit  IDictionary(IDictionary *set) {
        root = new BTree<Pair<TKey, TElement>>(set->root);
        size = set->size;
    }
    explicit IDictionary(BTree<Pair<TKey, TElement>> * tree) {
        root = new BTree<Pair<TKey, TElement>>(tree);
        size = root->Get_size();
    }
    size_t GetCount() {
        return size;
    }
    bool ContainsKey(TKey &key) {
        Pair<TKey, TElement> str;
        str.key = key;
        return root->Search(str);
    }
    void Remove(TKey &key) {
        Pair<TKey, TElement> str;
        str.key = key;
        if (!ContainsKey(key)) {
            throw runtime_error("Non_existent_element");
        }
        root->Remove(str);
        size--;
    }

    TElement Get(TKey &key) {
        if (!ContainsKey(key)) {
            throw runtime_error("Non_existent_element");
        }
        Pair<TKey, TElement> str;
        str.key = key;
        return root->Get(str).value;
    }
    void Add(TKey key, TElement value) {
        Pair<TKey, TElement> str;
        str.key = key;
        str.value = value;
        if (ContainsKey(key)) {
            root->Remove(str);
        } else {
            size++;
        }
        root->Insert(str);
    }
    ArraySequence<Pair<TKey, TElement>> * Get_all() {
        return root->Get_all();
    }
};

#endif //MEPHI_SEM3_LAB2_IDICTIONARY_HPP
