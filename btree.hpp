#ifndef MEPHI_SEM3_LAB2_BTREE_HPP
#define MEPHI_SEM3_LAB2_BTREE_HPP
#include "../mephi_sem2_lab2/array_sequence.hpp"
#include <iostream>
#include <fstream>

template <typename Type>
class BTree {
private:
    struct Node {
        Sequence<Type> *keys;
        Sequence<Node *> *children;
        size_t size;
        bool leaf;

        Node(size_t _degree, bool _leaf) {
            leaf = _leaf;
            keys = new ArraySequence<Type>(2 * _degree - 1);
            children = new ArraySequence<Node *>(2 * _degree);
            size = 0;
        }
    };

    // *****************************************************************
    Node *root;
    size_t degree;
    size_t t_size;
    int (*compare)(const Type&, const Type&);
    // *****************************************************************
    // Insert
    void insert_not_full(Node *node, Type key);
    void splitChild(Node *node, int ind);
    // removing
    void remove_node(Node *node, Type &key);
    int find_key_index(Node *node, Type &key);
    void remove_leaf(Node *node, int ind);
    void remove_not_leaf(Node *node, int ind);
    Type getPredecessor(Node *node, int ind);
    Type getSuccessor(Node *node, int ind);
    void fill(Node *node, int ind);
    void borrow_prev(Node *node, int ind);
    void borrow_next(Node *node, int ind);
    void merge(Node *node, int ind);
    // printing
    void print(Node * node);
    void dfs2(Node *ptr, std::ofstream &out);
    void invalid_print_node(Node *node);
    // Search node
    Node *search_keys_node(Node *node, Type &key);
    void copy(Node * build, Node * cur);
    void Get_size(Node * node, size_t &count);
    void Get_all(Node * node, ArraySequence<Type> * seq, size_t &ind);
public:

    explicit BTree(size_t _degree, int (*cmp)(const Type &i, const Type& j)) {
        root = nullptr;
        degree = _degree;
        compare = cmp;
        t_size = 0;
    }
    explicit BTree(BTree *node) {
        t_size = node->t_size;
        degree = node->degree;
        compare = node->compare;
        if (node == nullptr) return;
        root = new Node(degree, node->root->leaf);
        root->keys = node->root->keys->Copy();
        root->size = node->root->size;
        copy(root, node->root);
    }

    void Insert(Type key);
    void Remove(Type &key);
    bool Search(Type &key);
    Type Get(Type &key) {
        Node * find = search_keys_node(root, key);
        if (find) {
            return (find->keys->Get(find_key_index(find, key)));
        } else {
            throw runtime_error("Nod exist key");
        }
    }
    ArraySequence<Type> * Get_all() {
        auto seq = new ArraySequence<Type>(Get_size());
        size_t i = 0;
        Get_all(root, seq, i);
        return seq;
    }

    void Invalid_print() {
        if (!root) return;
        invalid_print_node(root);
    }
    int MakeGraphViz();
    void Print() {
        if (root != nullptr)
            print(root);
    }
    size_t Get_size() {
        return t_size;
    }
    Type Get_max();
    Type Get_min();
};

template <typename Type>
void BTree<Type>::Get_size(BTree::Node * node, size_t &count) {
    count += node->size;
    if (node->leaf) return;
    for (int i = 0; i <= node->size; ++i) {
        Get_size(node->children->Get(i), count);
    }
}

template <typename Type>
void BTree<Type>::copy(BTree::Node *build, BTree::Node *cur) {
    if (build->leaf) return;
    for (int i = 0; i <= cur->size; ++i) {
        build->children->Set(new Node(degree, cur->children->Get(i)->leaf), i);
        build->children->Get(i)->keys = cur->children->Get(i)->keys->Copy();
        build->children->Get(i)->size = cur->children->Get(i)->size;

        copy(build->children->Get(i), cur->children->Get(i));
    }
}

// **********************  Insert ************************************
template <typename Type>
void BTree<Type>::Insert(Type key) {
    if (root == nullptr) {
        root = new Node(degree, true);
    }
    if (root->size == 2 * degree - 1) {
        Node *s = new Node(degree, false);
        s->children->Set(root, 0);
        root = s;
        splitChild(root, 0);
    }
    insert_not_full(root, key);
    t_size++;
}

template <typename Type>
void BTree<Type>::insert_not_full(Node *node, Type key) {
    int i = node->size - 1;
    if (node->leaf) {
        while (i >= 0 && compare(node->keys->Get(i), key) > 0) {
            node->keys->Set(node->keys->Get(i), i + 1);
            i--;
        }

        node->keys->Set(key, i + 1);
        node->size = node->size + 1;
    } else {
        while (i >= 0 && compare(node->keys->Get(i), key) > 0) i--;
        ++i;
        if (node->children->Get(i)->size == 2 * degree - 1) {
            splitChild(node, i);

            if (compare(node->keys->Get(i), key) < 0)
                i++;
        }
        insert_not_full(node->children->Get(i), key);
    }
}

template <typename Type>
void BTree<Type>::splitChild(Node *node, int ind) {
    Node *left = node->children->Get(ind);
    Node *right = new Node(degree, left->leaf);
    right->size = degree - 1;
    for (int i = 0; i < degree - 1; ++i)
        right->keys->Set(left->keys->Get(i + degree), i);
    if (!left->leaf)
        for (int i = 0; i < degree; ++i)
            right->children->Set(left->children->Get(i + degree), i);
    left->size = degree - 1;
    for (int i = node->size; i > ind; --i)
        node->children->Set(node->children->Get(i), i + 1);
    node->children->Set(right, ind + 1);
    if (node->size)
        for (int i = node->size - 1; i >= ind; --i)
            node->keys->Set(node->keys->Get(i), i + 1);
    node->size += 1;
    node->keys->Set(left->keys->Get(degree - 1), ind);
}
// **********************  Insert ************************************

template <typename Type>
int BTree<Type>::find_key_index(Node *node, Type &key) {
    int ind = 0;
    while (ind < node->size && compare(node->keys->Get(ind), key) < 0)
        ++ind;
    return ind;
}

template <typename Type>
typename BTree<Type>::Node *BTree<Type>::search_keys_node(BTree::Node *node, Type &key) {
    int i = 0;
    while (i < node->size && compare(node->keys->Get(i), key) < 0) ++i;
    if (i < node->size && compare(node->keys->Get(i), key) == 0) return node;
    if (node->leaf) {
        return nullptr;
    } else {
        return search_keys_node(node->children->Get(i), key);
    }
}

template <typename Type>
bool BTree<Type>::Search(Type &key) {
    if (root) {
        return search_keys_node(root, key);
    } else {
        return false;
    }
}

// **********************  removing  ************************************

template <typename Type>
void BTree<Type>::Remove(Type &key) {
    if (!root) {
        std::cout << "The tree is empty\n";
        return;
    }

    remove_node(root, key);

    if (root->size == 0) {
        Node *tmp = root;
        if (root->leaf)
            root = nullptr;
        else
            root = root->children->Get(0);

        delete tmp;
    }
}

template <typename Type>
void BTree<Type>::remove_node(Node *node, Type &key) {
    int ind = find_key_index(node, key);

    if (ind < node->size && compare(node->keys->Get(ind),key) == 0) {
        if (node->leaf)
            remove_leaf(node, ind);
        else
            remove_not_leaf(node, ind);
        t_size--;
    } else {
        if (node->leaf) {
//            std::cout << "The key " << key << " is does not exist in the tree\n";
            return;
        }

        bool flag = (ind == node->size);

        if (node->children->Get(ind)->size < degree)
            fill(node, ind);

        if (flag && ind > node->size)
            remove_node(node->children->Get(ind - 1), key);
        else
            remove_node(node->children->Get(ind), key);
    }
}

template <typename Type>
void BTree<Type>::remove_leaf(Node *node,int ind) {
    for (int i = ind + 1; i < node->size; ++i)
        node->keys->Set(node->keys->Get(i), i - 1);

    node->size--;
}

template <typename Type>
void BTree<Type>::remove_not_leaf(Node* node,int ind) {
    Type key = node->keys->Get(ind);

    if (node->children->Get(ind)->size >= degree) {
        Type pred = getPredecessor(node, ind);
        node->keys->Set(pred, ind);
        remove_node(node->children->Get(ind), pred);
    }

    else if (node->children->Get(ind + 1)->size >= degree) {
        Type succ = getSuccessor(node, ind);
        node->keys->Set(succ, ind);
        remove_node(node->children->Get(ind + 1), succ);
    }

    else {
        merge(node, ind);
        remove_node(node->children->Get(ind), key);
    }
}

template <typename Type>
Type BTree<Type>::getPredecessor(Node *node, int ind) {
    Node *cur = node->children->Get(ind);
    while (!cur->leaf)
        cur = cur->children->Get(cur->size);

    return cur->keys->Get(cur->size - 1);
}

template <typename Type>
Type BTree<Type>::getSuccessor(Node *node, int ind) {
    Node *curent = node->children->Get(ind + 1);
    while (!curent->leaf)
        curent = curent->children->Get(0);

    return curent->keys->Get(0);
}

template <typename Type>
void BTree<Type>::fill(Node *node, int ind) {
    if (ind != 0 && node->children->Get(ind - 1)->size >= degree)
        borrow_prev(node, ind);

    else if (ind != node->size && node->children->Get(ind + 1)->size >= degree)
        borrow_next(node, ind);

    else {
        if (ind != node->size)
            merge(node, ind);
        else
            merge(node, ind - 1);
    }
}

template <typename Type>
void BTree<Type>::borrow_prev(Node *node, int ind) {
    Node *child = node->children->Get(ind);
    Node *sibling = node->children->Get(ind - 1);

    for (int i = child->size - 1; i >= 0; --i)
        child->keys->Set(child->keys->Get(i), i + 1);

    if (!child->leaf) {
        for (int i = child->size; i >= 0; --i)
            child->children->Set(child->children->Get(i), i + 1);
    }

    child->keys->Set(node->keys->Get(ind - 1), 0);

    if (!child->leaf)
        child->children->Set(sibling->children->Get(sibling->size), 0);

    node->keys->Set(sibling->keys->Get(sibling->size - 1), ind - 1);

    child->size += 1;
    sibling->size -= 1;

}

template <typename Type>
void BTree<Type>::borrow_next(Node *node, int ind) {
    Node *child = node->children->Get(ind);
    Node *sibling = node->children->Get(ind + 1);

    child->keys->Set(node->keys->Get(ind), child->size);

    if (!(child->leaf))
        child->children->Set(sibling->children->Get(0), child->size + 1);

    node->keys->Set(sibling->keys->Get(0), ind);

    for (int i = 1; i < sibling->size; ++i)
        sibling->keys->Set(sibling->keys->Get(i), i - 1);

    if (!sibling->leaf) {
        for (int i = 1; i <= sibling->size; ++i)
            sibling->children->Set(sibling->children->Get(i), i - 1);
    }

    child->size += 1;
    sibling->size -= 1;

}

template <typename Type>
void BTree<Type>::merge(Node *node, int ind) {
    Node *child = node->children->Get(ind);
    Node *sibling = node->children->Get(ind + 1);

    child->keys->Set(node->keys->Get(ind), degree - 1);

    for (int i = 0; i < sibling->size; ++i)
        child->keys->Set(sibling->keys->Get(i), i + degree);

    if (!child->leaf) {
        for (int i = 0; i <= sibling->size; ++i)
            child->children->Set(sibling->children->Get(i), i + degree);
    }

    for (int i = ind + 1; i < node->size; ++i)
        node->keys->Set(node->keys->Get(i), i - 1);

    for (int i = ind + 2; i <= node->size; ++i)
        node->children->Set(node->children->Get(i), i - 1);

    child->size += sibling->size + 1;
    node->size--;

    delete (sibling);
}

// **********************  removing  ************************************

template <typename Type>
void BTree<Type>::dfs2(Node *ptr, std::ofstream &out) {
    if (!ptr) {
        return;
    }
    int size = ptr->size;

    out << "struct" << ptr<< " [label=\"";

    for (int i = 0; i < size - 1; ++i) {
        out << "<f" << i << "> " << ptr->keys->Get(i) << "|";
    }
    out << "<f" << size - 1 << "> " << ptr->keys->Get(size - 1) << "\"];\n";

    if (ptr->leaf) return;

    out << "struct" << ptr << ":f0 -- struct" << ptr->children->Get(0) << ":f"<< ((ptr->children->Get(0)->size + 1)/ 2 - 1) <<";\n";
    dfs2(ptr->children->Get(0), out);
    for (int i = 1; i <= size; ++i) {
        out << "struct" << ptr << ":f"<< i - 1 <<" -- struct" << ptr->children->Get(i) << ":f"<< ((ptr->children->Get(i)->size + 1)/ 2 - 1) <<";\n";
        dfs2(ptr->children->Get(i), out);
    }
}

template <typename Type>
int BTree<Type>::MakeGraphViz() {
    if (root == nullptr) {
        return 1;
    }
    std::ofstream out;
    out.open("graph.gv");
    out << "graph G {\n" << "node [shape=record];\n";

    dfs2(root, out);

    out << "}";
    out.close();

    system(((string)"dot -Tpng graph.gv -o BTree.png").c_str());
    system(((string)"rm graph.gv").c_str());
    return 0;
}

template <typename Type>
void BTree<Type>::print(Node *node) {
    int i;
    for (i = 0; i < node->size; i++) {
        if (!node->leaf)
            print(node->children->Get(i));
        std::cout << " " << node->keys->Get(i);
    }

    if (!node->leaf)
        print(node->children->Get(i));
}

template <typename Type>
void BTree<Type>::Get_all(BTree::Node *node, ArraySequence<Type> *seq, size_t &ind) {
    int i;
    for (i = 0; i < node->size; i++) {
        if (!node->leaf)
            Get_all(node->children->Get(i), seq, ind);
        seq->Set(node->keys->Get(i), ind);
        ind++;
    }

    if (!node->leaf)
        Get_all(node->children->Get(i), seq, ind);
}

template <typename Type>
void BTree<Type>::invalid_print_node(BTree::Node *node) {
    if (!node->leaf) {
        for (int i = 0; i < node->size + 1; ++i) {
            invalid_print_node(node->children->Get(i));
        }
    }
    cout << "size: " << node->size << " leaf: " << node->leaf;
    if (node == root)
        cout << " Root";
    cout << endl;
    for (int i = 0; i < node->size; ++i) {
        cout << node->keys->Get(i) << endl;
    }
    cout << endl;
}

template <typename Type>
Type BTree<Type>::Get_min() {
    if (root->leaf)
        return root->keys->Get(0);
    return getSuccessor(root, -1);
}

template <typename Type>
Type BTree<Type>::Get_max() {
    if (root->leaf)
        return root->keys->Get(root->size - 1);
    return getPredecessor(root, root->size);
}


#endif //MEPHI_SEM3_LAB2_BTREE_HPP
