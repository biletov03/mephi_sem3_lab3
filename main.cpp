#include "btree.hpp"
#include "IDictionary.hpp"
#include "../mephi_sem3_lab1/Sorts.hpp"
#include <iostream>
#include <unistd.h>
#include <chrono>
#include <cstdlib>

using namespace std;

namespace menu {
    size_t degree = 3;
    size_t count = 1;
    void types() {
        printf("\x1B[2J\x1B[H");
        printf("0) Tests\n");
        printf("1) Add element\n");
        printf("2) Add many elements\n");
        printf("3) Remove element\n");
        printf("4) Search element\n");
        printf("5) Print all elements\n");
        printf("6) Print all nodes\n");
        printf("7) Print max\n");
        printf("8) Print min\n");
        printf("9) Make visualisation\n");
        printf("10) Exit\n");
    }
    int compare_int(const int& i, const int& j) {
        return i - j;
    }

    int compare_float(const float& i, const float& j) {
        return (int)(i - j);
    }

    int compare_pair(const Pair<std::string, ArraySequence<int> *>& i, const Pair<std::string, ArraySequence<int> *>& j) {
        return j.value->GetLength() - i.value->GetLength();
    }
}

int main() {
    system(((string)"rm -rf *.png").c_str());
    size_t type;
    int element, count;
    char c;
    printf("\x1B[2J\x1B[H");
    printf("Choose degree: ");
    cin >> menu::degree;
    auto * root = new BTree<int>(menu::degree, menu::compare_int);
    do {
        menu::types();
        cin >> type;
        printf("\x1B[2J\x1B[H");
        switch (type) {
            case (0):
                printf("\x1B[2J\x1B[H");
                system(((string) "g++ -std=c++11 "
                                 "../CodingSandboxAllLingos/TestingWithCatch/*.cpp -o "
                                 "res.exe")
                               .c_str());
                system(((string) "./res.exe").c_str());
                system(((string) "rm res.exe").c_str());
                read(0, &c, 1);
                break;
            case (1):
                printf("Input element: ");
                cin >> element;
                root->Insert(element);
                break;
            case (2):
                printf("Input amount: ");
                cin >> element;
                printf("Input max size: ");
                cin >> count;
                for (int i = 0; i < element; ++i) {
                    root->Insert((int)rand() % (count + 1));
                }
                break;
            case (3):
                printf("Remove element: ");
                cin >> element;
                root->Remove(element);
                break;
            case (4):
                printf("Search element: ");
                cin >> element;
                if (root->Search(element))
                    cout << "We have key: " << element;
                else
                    cout << "We do not have key: " << element;
                cout << endl;
                read(0, &c, 1);
                break;
            case (5):
                cout << "Btree elements:";
                root->Print();
                cout << endl;
                read(0, &c, 1);
                break;
            case (6):
                root->Invalid_print();
                read(0, &c, 1);
                break;
            case (7):
                cout << "Max element: " << root->Get_max();
                cout << endl;
                read(0, &c, 1);
                break;
            case (8):
                cout << "Min element: " << root->Get_min();
                cout << endl;
                read(0, &c, 1);
                break;
            case (9):
                root->MakeGraphViz();
                system(((string)"mv BTree.png " + "BTree" + to_string(menu::count) + ".png").c_str());
                cout << "Graph in file: BTree" <<  to_string(menu::count) << ".png" << endl;
                read(0, &c, 1);
                menu::count++;
                break;
            case(10):
                break;
            default:
                printf("\x1B[2J\x1B[H");
                printf("Invalid input\n");
                sleep(1);
        }
    } while (type != 10);
    return 0;
}

/*
int main() {
//    auto root = new BTree<Pair<int, int>>(3, compare_pair_int);
//    Pair<int, int> p{};
//    for (int i = 0; i < 100; ++i) {
//        p.key = i;
//        p.value = 100 - i;
//        root->Insert(p);
//    }
//    auto dic = new IDictionary<int, int>(root);
//    cout << dic->GetCount() << endl;
//    for (int i = 0; i < 100; ++i) {
//        cout << dic->ContainsKey(i);
//    }
//    for (int i = 0; i < 100; ++i) {
//        cout << dic->Get(i) << " ";
//    }
//    int n = 0;
//    int d = 50;
//    dic->Remove(n);
//    cout << endl;
//    dic->Add(n, d);
//    for (int i = 0; i < 100; ++i) {
//        if (dic->ContainsKey(i)) {
//            cout << dic->Get(i) << " ";
//        }
//    }
//    cout << endl;
//    d = 51;
//    dic->Add(n, d);
//    for (int i = 0; i < 100; ++i) {
//        if (dic->ContainsKey(i)) {
//            cout << dic->Get(i) << " ";
//        }
//    }
    auto dict = new IDictionary<std::string, ArraySequence<int> *>(3, compare_str_pair);
    auto quick = new QuickSort<Pair<std::string, ArraySequence<int> *>>;
    std::string s, ss;
    int max = 3, min = 1;
    s = "abeedcadaedacadccba";
    for (int i = 0; i <= s.size(); ++i) {
        for(int j = min; j <= max; ++j) {
            if (i + j < s.size()){
                ss = s.substr(i, j);
                if (!dict->ContainsKey(ss)) {
                    dict->Add(ss, new ArraySequence<int>());
                }
                dict->Get(ss)->Append(i);
            }
        }
    }
    auto res = quick->Sort(dict->Get_all(), menu::compare_pair);
    for (int i = 0; i < res->GetLength(); i++) {
        cout << res->Get(i).value->GetLength() << "  " << res->Get(i).key;
        cout << endl;
    }
    return 0;
}
*/